Ts = 1/50;
t = 0:Ts:10-Ts;                     
x = sin(15*t) + cos(18*t);
plot(t,x)
xlabel('Time (seconds)')
ylabel('Amplitude')
y = fft(x);   
fs = 1/Ts;
f = (0:length(y)-1)*fs/length(y);
plot(f,abs(y))
xlabel('Frequency (Hz)')
ylabel('Magnitude')
title('Magnitude')
