%[old_path]=which('rdsamp'); if(~isempty(old_path)) rmpath(old_path(1:end-8)); end
%wfdb_url='https://physionet.org/physiotools/matlab/wfdb-app-matlab/wfdb-app-toolbox-0-10-0.zip';
%[filestr,status] = urlwrite(wfdb_url,'wfdb-app-toolbox-0-10-0.zip');
%unzip('wfdb-app-toolbox-0-10-0.zip');
%cd mcode
%addpath(pwd)
%savepath
cd D:\EGC\ptb-xl-a-large-publicly-available-electrocardiography-dataset-1.0.1\records100\00000
%d C:\Users\Evgeny\Downloads\ptb-xl-a-large-publicly-available-electrocardiography-dataset-1.0.1\ptb-xl-a-large-publicly-available-electrocardiography-dataset-1.0.1\records100\00000
clc;
clear;
%Learning signals NORM
signal=[];
signalun=[];
fs=[];
n = 1000;
[signal(1,1:1000,1:12), fs(1), tm] = rdsamp('00001_lr');%+
[signal(2,1:1000,1:12), fs(2), tm] = rdsamp('00002_lr');%-
[signal(3,1:1000,1:12), fs(3), tm] = rdsamp('00003_lr');%-
[signal(4,1:1000,1:12), fs(4), tm] = rdsamp('00004_lr');%+
[signal(5,1:1000,1:12), fs(5), tm] = rdsamp('00005_lr');%+
[signal(6,1:1000,1:12), fs(6), tm] = rdsamp('00006_lr');%-
[signal(7,1:1000,1:12), fs(7), tm] = rdsamp('00007_lr');%+
[signal(8,1:1000,1:12), fs(8), tm] = rdsamp('00009_lr');%-
[signal(9,1:1000,1:12), fs(9), tm] = rdsamp('00010_lr');%-
[signal(10,1:1000,1:12), fs(10), tm] = rdsamp('00011_lr');%-
[signal(11,1:1000,1:12), fs(11), tm] = rdsamp('00012_lr');%-
[signal(12,1:1000,1:12), fs(12), tm] = rdsamp('00013_lr');%-
[signal(13,1:1000,1:12), fs(13), tm] = rdsamp('00014_lr');%-
[signal(14,1:1000,1:12), fs(14), tm] = rdsamp('00015_lr');%+
[signal(15,1:1000,1:12), fs(15), tm] = rdsamp('00016_lr');%+
[signal(16,1:1000,1:12), fs(16), tm] = rdsamp('00019_lr');%+
[signal(17,1:1000,1:12), fs(17), tm] = rdsamp('00021_lr');%-
[signal(18,1:1000,1:12), fs(18), tm] = rdsamp('00024_lr');%-
[signal(19,1:1000,1:12), fs(19), tm] = rdsamp('00025_lr');%-
[signal(20,1:1000,1:12), fs(20), tm] = rdsamp('00027_lr');%-
[signal(21,1:1000,1:12), fs(21), tm] = rdsamp('00029_lr');%+
[signal(22,1:1000,1:12), fs(22), tm] = rdsamp('00031_lr');%-
[signal(23,1:1000,1:12), fs(23), tm] = rdsamp('00033_lr');%-
[signal(24,1:1000,1:12), fs(24), tm] = rdsamp('00035_lr');%-
[signal(25,1:1000,1:12), fs(25), tm] = rdsamp('00036_lr');%-
[signal(26,1:1000,1:12), fs(26), tm] = rdsamp('00037_lr');%-
[signal(27,1:1000,1:12), fs(27), tm] = rdsamp('00038_lr');%-
[signal(28,1:1000,1:12), fs(28), tm] = rdsamp('00040_lr');%-
[signal(29,1:1000,1:12), fs(29), tm] = rdsamp('00042_lr');%+
[signal(30,1:1000,1:12), fs(30), tm] = rdsamp('00043_lr');%-
[signal(31,1:1000,1:12), fs(31), tm] = rdsamp('00044_lr');%-
[signal(32,1:1000,1:12), fs(32), tm] = rdsamp('00046_lr');%-
[signal(33,1:1000,1:12), fs(33), tm] = rdsamp('00047_lr');%+
[signal(34,1:1000,1:12), fs(34), tm] = rdsamp('00051_lr');%+
[signal(35,1:1000,1:12), fs(35), tm] = rdsamp('00053_lr');%+
[signal(36,1:1000,1:12), fs(36), tm] = rdsamp('00055_lr');%-
[signal(37,1:1000,1:12), fs(37), tm] = rdsamp('00056_lr');%+
[signal(38,1:1000,1:12), fs(38), tm] = rdsamp('00057_lr');%+
[signal(39,1:1000,1:12), fs(39), tm] = rdsamp('00058_lr');%-
[signal(40,1:1000,1:12), fs(40), tm] = rdsamp('00059_lr');%-
[signal(41,1:1000,1:12), fs(41), tm] = rdsamp('00060_lr');%-
[signal(42,1:1000,1:12), fs(42), tm] = rdsamp('00061_lr');%-
[signal(43,1:1000,1:12), fs(43), tm] = rdsamp('00062_lr');%+
[signal(44,1:1000,1:12), fs(44), tm] = rdsamp('00064_lr');%+
[signal(45,1:1000,1:12), fs(45), tm] = rdsamp('00066_lr');%+
[signal(46,1:1000,1:12), fs(46), tm] = rdsamp('00067_lr');%-
[signal(47,1:1000,1:12), fs(47), tm] = rdsamp('00068_lr');%-
[signal(48,1:1000,1:12), fs(48), tm] = rdsamp('00069_lr');%-
[signal(49,1:1000,1:12), fs(49), tm] = rdsamp('00070_lr');%+
[signal(50,1:1000,1:12), fs(50), tm] = rdsamp('00071_lr');%+
[signal(51,1:1000,1:12), fs(51), tm] = rdsamp('00072_lr');%-
%19
%Learning signals IMI

[signalun(1,1:1000,1:12), fsu(1), tm] = rdsamp('00008_lr');%+
[signalun(2,1:1000,1:12), fsu(2), tm] = rdsamp('00039_lr');%-

[signalun(3,1:1000,1:12), fsu(3), tm] = rdsamp('00103_lr');%-
[signalun(4,1:1000,1:12), fsu(4), tm] = rdsamp('00139_lr');%-
[signalun(5,1:1000,1:12), fsu(5), tm] = rdsamp('00142_lr');%-
[signalun(6,1:1000,1:12), fsu(6), tm] = rdsamp('00146_lr');%+
[signalun(7,1:1000,1:12), fsu(7), tm] = rdsamp('00153_lr');%+
[signalun(8,1:1000,1:12), fsu(8), tm] = rdsamp('00161_lr');%-
[signalun(9,1:1000,1:12), fsu(9), tm] = rdsamp('00175_lr');%-
[signalun(10,1:1000,1:12), fsu(10), tm] = rdsamp('00181_lr');%-
[signalun(11,1:1000,1:12), fsu(11), tm] = rdsamp('00210_lr');%+
[signalun(12,1:1000,1:12), fsu(12), tm] = rdsamp('00234_lr');%+
[signalun(13,1:1000,1:12), fsu(13), tm] = rdsamp('00240_lr');%-
[signalun(14,1:1000,1:12), fsu(14), tm] = rdsamp('00257_lr');%-
[signalun(15,1:1000,1:12), fsu(15), tm] = rdsamp('00258_lr');%-
[signalun(16,1:1000,1:12), fsu(16), tm] = rdsamp('00266_lr');%-
[signalun(17,1:1000,1:12), fsu(17), tm] = rdsamp('00267_lr');%-
[signalun(18,1:1000,1:12), fsu(18), tm] = rdsamp('00269_lr');%-
[signalun(19,1:1000,1:12), fsu(19), tm] = rdsamp('00270_lr');%-
[signalun(20,1:1000,1:12), fsu(20), tm] = rdsamp('00281_lr');%-
[signalun(21,1:1000,1:12), fsu(21), tm] = rdsamp('00290_lr');%-
[signalun(22,1:1000,1:12), fsu(22), tm] = rdsamp('00323_lr');%-
[signalun(23,1:1000,1:12), fsu(23), tm] = rdsamp('00325_lr');%+
[signalun(24,1:1000,1:12), fsu(24), tm] = rdsamp('00337_lr');%+
[signalun(25,1:1000,1:12), fsu(25), tm] = rdsamp('00380_lr');%+
[signalun(26,1:1000,1:12), fsu(26), tm] = rdsamp('00383_lr');%-
[signalun(27,1:1000,1:12), fsu(27), tm] = rdsamp('00407_lr');%+
[signalun(28,1:1000,1:12), fsu(28), tm] = rdsamp('00423_lr');%-
[signalun(29,1:1000,1:12), fsu(29), tm] = rdsamp('00429_lr');%-
[signalun(30,1:1000,1:12), fsu(30), tm] = rdsamp('00442_lr');%-
[signalun(31,1:1000,1:12), fsu(31), tm] = rdsamp('00453_lr');%-
[signalun(32,1:1000,1:12), fsu(32), tm] = rdsamp('00477_lr');%-
[signalun(33,1:1000,1:12), fsu(33), tm] = rdsamp('00482_lr');%-
[signalun(34,1:1000,1:12), fsu(34), tm] = rdsamp('00483_lr');%-
[signalun(35,1:1000,1:12), fsu(35), tm] = rdsamp('00486_lr');%-
[signalun(36,1:1000,1:12), fsu(36), tm] = rdsamp('00496_lr');%-
[signalun(37,1:1000,1:12), fsu(37), tm] = rdsamp('00500_lr');%-
[signalun(38,1:1000,1:12), fsu(38), tm] = rdsamp('00513_lr');%-
[signalun(39,1:1000,1:12), fsu(39), tm] = rdsamp('00532_lr');%-
[signalun(40,1:1000,1:12), fsu(40), tm] = rdsamp('00540_lr');%-
[signalun(41,1:1000,1:12), fsu(41), tm] = rdsamp('00544_lr');%-
[signalun(42,1:1000,1:12), fsu(42), tm] = rdsamp('00545_lr');%-
[signalun(43,1:1000,1:12), fsu(43), tm] = rdsamp('00554_lr');%-
[signalun(44,1:1000,1:12), fsu(44), tm] = rdsamp('00558_lr');%-
[signalun(45,1:1000,1:12), fsu(45), tm] = rdsamp('00563_lr');%-
[signalun(46,1:1000,1:12), fsu(46), tm] = rdsamp('00567_lr');%-
[signalun(47,1:1000,1:12), fsu(47), tm] = rdsamp('00579_lr');%-
[signalun(48,1:1000,1:12), fsu(48), tm] = rdsamp('00600_lr');%-
[signalun(49,1:1000,1:12), fsu(49), tm] = rdsamp('00601_lr');%-
[signalun(50,1:1000,1:12), fsu(50), tm] = rdsamp('00604_lr');%-
[signalun(51,1:1000,1:12), fsu(51), tm] = rdsamp('00610_lr');%-
%9
t=[-2;2];
net = newc(minmax(t),2);
net.inputs{1}.size = n;
net = init(net);
s=[];
su=[];
g=zeros(n,12);
gu=zeros(n,12);
    for jj=1:1:51
        for jjj=1:n
        for k=1:12
            s(jjj,k) = signal(jj,jjj,k);
            g(jjj,k) = g(jjj,k)+s(jjj,k);
            su(jjj,k) = signalun(jj,jjj,k);

            gu(jjj,k) = gu(jjj,k)+su(jjj,k);
        end
        end
        if jj==2
            figure
            plot(s(:,1)); grid
            z=smoothdata(s);
            x=sgolayfilt(z,0,15);
            figure
            plot(x(:,1)); grid
            [pks,locs] = findpeaks(x(:,1));
            text(locs+.02,pks,num2str((1:numel(pks))'))
            [pks2,locs2] =  findpeaks(pks);
            figure 
            plot(pks),grid
            first_max_value = max(pks)
            second_max_value = max(pks(pks~=max(pks)))
            B = sort(pks,'descend')
            third_max_value = B(3)
        end
        g=g/n;
        gu=gu/n;
        z=smoothdata(g);
        zu=smoothdata(gu);
        x=sgolayfilt(z,0,15);
        xu=sgolayfilt(zu,0,15);
   
    end;
    %figure
    %plot(x)
    %figure
    %plot(xu)
        net.trainParam.epochs = 55;
        [net,tr]=train(net,x,[1 1 1 1 1 1 1 1 1 1 1 1 ;0 0 0 0 0 0 0 0 0 0 0 0]);

%figure
%plot(tm(1:n),net.IW{1})
        net.trainParam.epochs = 45;
        [net,tr]=train(net,xu,[0 0 0 0 0 0 0 0 0 0 0 0;1 1 1 1 1 1 1 1 1 1 1 1]) ;
%figure
%plot(tm(1:n),net.IW{1})
        
  %  end
count1 = 0;
count2 = 0;
    for jj=1:1:51
        for jjj=1:n
        for k=1:12
            s(jjj,k) = signal(jj,jjj,k);
            su(jjj,k) = signalun(jj,jjj,k);
        end
        end
        z=smoothdata(s);
        zu=smoothdata(su);
        x=sgolayfilt(z,0,15);
        xu=sgolayfilt(zu,0,15);

        disp('перший:');
        y=sim(net,x);
        count = 0;
        for j=1:2:24
            if y(j) == 1
                count=count+1;
            end
        end
        if count > 6
            count1=count1+1;
            disp('перший=');
            jj
        end
        disp('второй:');
        y2=sim(net,xu);
        count = 0;
        for j=2:2:25
            if y2(j) == 1
                count=count+1;
            end
        end
        if count > 6
            count2=count2+1;
            disp('второй=');
            jj
        end
    end
count1
count2
